import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

import net.sourceforge.tess4j.Tesseract;

public class Reconocimiento {
	public static void main(String[] args) {
		Reconocimiento test = new Reconocimiento();
		Tesseract tesseract = new Tesseract();
		
        try {
            tesseract.setDatapath(".\\tessdata");
            String text = tesseract.doOCR(new File("ejemplos\\Text5.png"));
  
            System.out.print(text);
            
            test.PorcentajeSimilitud(text);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
	}
	
	public void PorcentajeSimilitud(String txt_reconocimiento) {
		int cont = 0;
		float porcentaje;
		
		String txt_original = ObtenerOriginal();
		ArrayList<String> texto1 = CrearListas(txt_original);
		ArrayList<String> texto2 = CrearListas(txt_reconocimiento);
		
		for(int i = 0; i < texto1.size(); i++) {
			int minimo = Math.min(texto1.get(i).length(), texto2.get(i).length());
			
			for (int j = 0; j < minimo; j++) {
				char char1 = texto1.get(i).charAt(j);
				char char2 = texto2.get(i).charAt(j);
				
				if(char1 == char2) {
					cont++;
				}
			}			
		}
		
		porcentaje = ((texto2.size() + cont) * 100)/txt_original.length();
		
		System.out.println("El porcentaje de reconocimiento es de " + porcentaje + " %");
	}
	
	public ArrayList<String> CrearListas(String texto){
		ArrayList<String> txt = new ArrayList<String>();
		int aux = 0;
		
		for(int i = 0; i < texto.length(); i++) {
			if(texto.substring(i, i + 1).equals(" ")) {
				txt.add(texto.substring(aux, i));
				aux = i + 1;
			}
		}
		
		txt.add(texto.substring(aux, texto.length() - 1));
		
		return txt;
	}
	
	public String ObtenerOriginal() {
		File texto_original = new File("ejemplos\\texto1.txt");
        Scanner sc;
		try {
			sc = new Scanner(texto_original);
			
			String original = "";
	        while(sc.hasNextLine()) {
	        	original = original + sc.nextLine() + "\n";
	        }
	        original = original + "\n";
	        
	        return original;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}                
	}
}
